// definizione del metodo dei trapezi per l'integrazione

#include <cmath>

double fTrap ( double x )
{
	return 1-exp(x/2.0);
}


double Trapezi ( double a, double b, int n )
{
	double dx = (b - a)/n;
	
	double * yi = new double[n + 1];
	
	for ( int i = 0; i <= n; i++ )
		yi[i] = fTrap( a + i*dx );
	
	double sum = yi[0] / 2;
	
	for ( int j = 1; j < n; j++ )
		sum = sum + yi[j];
	
	sum = sum + yi[n] / 2;
	
	delete [] yi;
	
	return dx * sum;
	
}

#include <iostream>

using std::cout;
using std::cin;
using std::endl;

#include <cmath>

int main()
{
	
	int a;
	int b;
	
	cout << "Calcolo di un integrale con il metodo dei trapezi " << endl;
	cout << "Dimmi gli estremi di integrazione " << endl;
	cin >> a >> b;
	
	int n;
	
	cout << "Dimmi il numero di sottointervalli " << endl;
	cin >> n;
	
	double risultato = Trapezi ( a, b, n );
	
	cout << "Ecco l'integrale:  " << risultato << endl;
	
	return 0;
	
}
