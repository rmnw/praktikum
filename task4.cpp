#include <cstdlib>
#include <cstdio>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <time.h>
#include <iostream>
#include <fstream>
#include <math.h> 

#define N 1000000
#define T 1
#define delta_T 1
#define mu 0.1
#define sigma 0.2
#define start 10
#define K 10

using namespace std;

int main()
{
	gsl_rng* r = gsl_rng_alloc(gsl_rng_mt19937);
	gsl_rng_set(r, time(NULL));
	ofstream f;
	f.open("task4_4.txt", ios::trunc);
	
	int M = T / delta_T;
	double W[M + 1];
	double S[M + 1];
	W[0] = 0;
	S[0] = start;
	double sum = 0;
	double alpha = 0, beta = 0, variance;
	int d = 0;
	for(int j = 0;j < N;j++) { 
		for(int i = 1;i < M + 1;i++) 
		{
			W[i] = W[i - 1] + sqrt(delta_T) * gsl_ran_gaussian(r, 1);
			S[i] = S[0] * exp((mu - 0.5*sigma*sigma)*i*delta_T + sigma*W[i]);
		}
		sum += S[M] > K ? S[M] - K : 0;
		if(j == pow(10, d))
		{
			f << j << " " << sum/j << "\n";
			d++;
		}
	}
	f << N << " " << sum/N << "\n";
	f.close();
	gsl_rng_free(r);
	
	return 0;
}

