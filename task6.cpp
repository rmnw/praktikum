#include <cstdlib>
#include <cstdio>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <time.h>
#include <iostream>
#include <fstream>
#include <math.h> 
#include <vector>

using namespace std;

vector< vector<int> > trapezoidal(int l)
{
	int N = pow(2, l) - 1;
	vector< vector<int> > temp(N, vector<int>(2,0));
	temp[0][1] = 123;
	return temp;
}

int main()
{
	vector< vector<int> > a = trapezoidal(2);
	std::cout << a[0][1];
}
